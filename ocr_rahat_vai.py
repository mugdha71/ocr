import cv2
import numpy as np
import os

from convert_to_main_form import getPage
from transform import four_point_transform


def get_contour_precedence(contour, cols):
    tolerance_factor = 50
    origin = cv2.boundingRect(contour)
    return ((origin[1] // tolerance_factor) * tolerance_factor) * cols + origin[0]


img = cv2.imread('demo2.jpg')
img = getPage(img)
# print(img.shape)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
edges = cv2.Canny(gray, 10, 150, apertureSize=3)
minLineLength = 100
maxLineGap = 10
lines = cv2.HoughLinesP(edges, 1, np.pi / 180, 100, minLineLength, maxLineGap)
for x1, y1, x2, y2 in lines[0]:
    cv2.line(img, (x1, y1), (x2, y2), (0, 0, 0), 2)
# imgray=cv2.cvtColor(im,cv2.COLOR_BGR2GRAY) # BGR to grayscale
#
ret, thresh = cv2.threshold(gray, 50, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
# (thresh, im_bw) = cv2.threshold(im_gray, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
_, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
contours.sort(key=lambda x: get_contour_precedence(x, img.shape[1]))
number = 1
user_id = 3
user_dir = 'user' + str(user_id) + '/'
# print(lines)
height, width = img.shape[:2]
x_lower = width * 0.03
x_upper = width * 0.1


list = []
for contour in contours:
    # bounding contour
    x, y, w, h = cv2.boundingRect(contour)
    if x_lower < w < x_upper:
        try:
            os.makedirs(user_dir)
        except OSError:
            pass
        list.append((x,y,w,h))
        crop_img = img[y:y + h, x:x + w]
        path = user_dir + str(number) + ".jpg"
        cv2.imwrite(path, crop_img)
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
        number = number + 1

# cv2.drawContours(im,countours,-1,(0,255,1),2)
print(list)
image = cv2.resize(img, (700, 700))
cv2.imshow("Contour", image)
cv2.waitKey(0)
cv2.destroyAllWindows()
