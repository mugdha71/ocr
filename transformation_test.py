import cv2
import numpy as np

from transform import four_point_transform

img = cv2.imread('demo6.jpg')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
edges = cv2.Canny(gray, 10, 150, apertureSize=3)
ed = cv2.resize(edges, (700, 700))
cv2.imshow("edge", ed)
# cv2.imshow("gray", gray)
minLineLength = 100
maxLineGap = 10

lines = cv2.HoughLinesP(edges, 1, np.pi / 180, 100, minLineLength, maxLineGap)
for x1, y1, x2, y2 in lines[0]:
    cv2.line(img, (x1, y1), (x2, y2), (0, 0, 0), 2)
# imgray=cv2.cvtColor(im,cv2.COLOR_BGR2GRAY) # BGR to grayscale
#
ret, thresh = cv2.threshold(gray, 50, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
# (thresh, im_bw) = cv2.threshold(im_gray, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
_, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

height = edges.shape[0]
width = edges.shape[1]
MAX_COUNTOUR_AREA = (width - 10) * (height - 10)
maxAreaFound = MAX_COUNTOUR_AREA * 0.5
# Page fill at least half of image, then saving max area found		maxAreaFound = MAX_COUNTOUR_AREA * 0.5
# Saving page contour
pageContour = np.array([[[5, 5], [5, height - 5], [width - 5, height - 5], [width - 5, 5]]])

for cnt in contours:
    # Simplify contour
    perimeter = cv2.arcLength(cnt, True)
    approx = cv2.approxPolyDP(cnt, 0.03 * perimeter, True)
    # Page has 4 corners and it is convex
    #  Page area must be bigger than maxAreaFound
    if (len(approx) == 4
            and cv2.isContourConvex(approx)
            and maxAreaFound < cv2.contourArea(approx) < MAX_COUNTOUR_AREA):
        maxAreaFound = cv2.contourArea(approx)
        pageContour = approx
print("PAGE CORNER ")
s = "["
print(pageContour)
for p in pageContour:
    s += "("
    s += str(p[0][0])
    s += ","
    s += str(p[0][1])
    s += ")"
    s += ","
s = s[:-1]
s += "]"
print(s)
pts = np.array(eval(s), dtype="float32")
warped = four_point_transform(img, pts)
warpedre = cv2.resize(warped, (700, 700))
imgre = cv2.resize(img, (700, 700))
# cv2.imshow("Original", img)
cv2.imshow("Warped", warpedre)
cv2.imwrite("demo7.jpg", warped)
cv2.imshow("Original", imgre)
cv2.waitKey(0)
